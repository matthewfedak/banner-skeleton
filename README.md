# Banner Skeleton

#### What is it?

A simple HTML/JS skeleton page for loading different assets based on date.

#### Why might I need it?

It is not uncommon for me to receive multiple banner assets for the same campaign week. For example:

  - "In cinemas on 3rd, watch trailer"
  - "In cinemas tomorrow, book now"
  - "In cinemas today, book now"
  - "In cinemas now, book now"

Setting up multiple campagins for each banner asset or changing the banner asset from day to day can be tiresome. Using the banner skeleton we can configure multiple banners assets at the same time.

#### How do I use?

Go to your local banners directory (assume you already have one) and download the skeleton to a new directory

```
    mkdir somenewfilm && cd somenewfilm && wget https://gitlab.com/matthewfedak/banner-skeleton/raw/master/index.html

```
  
I have a localhost setup with a banners directory inside where I test all banner assets. The structure is like so.

```
banners
    some-film/
        some-film-book-now/index.html
        some-film-watch-trailer/image.png
    some-other-film
        some-other-film-out-tomorrow/index.html
        some-other-film-out-today/index.html
        some-other-film-out-now/index.html

```

###### Edit the assets config section

No coding needed, just the configuration. 

```
var assetsConfig = {
    type: 'sky', // 'sky' or 'leader'
    assets: [
        {
            type: 'image', // image or iframe
            date: new Date('2016-11-24'), // asset will be active from this date
            url: 'some-film-watch-trailer/image.png' // full or relative path
        },
        {
            type: 'iframe',
            date: new Date('2016-12-18'), 
            url: 'some-film-book-now/index.html'
        }
    ]
};
```

###### TEST

Remember to add the target url after a single hash (#) on the end of the url.

```
http://localhost/banners/somenewfilm/index.html#http://odeon.com/some-film-page/222

```

To test how different banners will look on different dates you can add a date query string param to the url.

```
http://localhost/banners/somenewfilm/index.html?date=2016-12-01#http://odeon.com/some-film-page/222

```

###### Upload to the banner server:

To do this I have some shortcuts setup in my bash preferences so I only need to type from my main banners directory
    
```
scpBannerUp somenewfilm
```

The bash preferences I have in my .zshrc or .bashrc file are below. Remember to change the local path to your local path if you use this.

```
# Alias for banner server
alias banners='cd ~/Documents/sites/banners'
alias bannerserver='ssh git@odeon-cms.krankikom.de'
scpBannerUp() {
    # scp up the banner
    scp -r $1 git@odeon-cms.krankikom.de:/cbx/storage/odeon-uploads/banner/html5
}
```

#### Found a bug? Want to contribute?

Great [grab the repo here](https://gitlab.com/matthewfedak/banner-skeleton/) and send a merge request!
